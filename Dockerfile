ARG NODE_VERSION="16.14.2"
ARG ALPINE_VERSION="3.15"



#
## Base Stage
FROM node:${NODE_VERSION}-alpine${ALPINE_VERSION} AS base-stage
WORKDIR /app/game-filter
RUN apk add --no-cache tzdata


#
## Dependencies Stage
FROM base-stage AS dependencies-stage
COPY package.json package-lock.json ./
# 只安裝 production 相關模組，並複製出來，準備給 Release Stage 使用
RUN npm ci --production
RUN cp -R node_modules /production_node_modules
# prod & dev 模組全部安裝
RUN npm ci --also=dev


#
## Build Stage
FROM dependencies-stage AS build-stage
ENV NODE_ENV=production

COPY angular.json karma.conf.js tsconfig.app.json tsconfig.json tsconfig.server.json tsconfig.spec.json .browserslistrc package.json package-lock.json tailwind.config.js ./
COPY server.ts .
COPY src src

RUN npm run build:ssr


#
## Release Stage
FROM base-stage AS release-stage
ENV NODE_ENV=production

COPY --from=dependencies-stage /production_node_modules node_modules
COPY --from=build-stage /app/game-filter/dist dist

COPY angular.json karma.conf.js tsconfig.app.json tsconfig.json tsconfig.server.json tsconfig.spec.json .browserslistrc package.json package-lock.json tailwind.config.js ./

EXPOSE 4000
CMD []
ENTRYPOINT ["npm", "run", "serve:ssr"]
